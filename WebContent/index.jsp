<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:base_layout>
    <jsp:attribute name="head_area">
      <jsp:include page="/templates/head_area.html" />
    </jsp:attribute>
    
	<jsp:attribute name="style_area">
      .navbar-static-top {
  		margin-bottom: 0px !important;
	  }
    </jsp:attribute>
    
    <jsp:attribute name="page_navbar">
      <jsp:include page="/templates/page_navbar.jsp" />
    </jsp:attribute>
    
    <jsp:attribute name="page_header">
      <jsp:include page="/templates/page_header.html" />
    </jsp:attribute>
    
     
    <jsp:attribute name="content">
  		<!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
           
          <!-- <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140"> -->
          <div class='text-align-center'>
            <i class="fa fa-film custom-fa-size"></i>
          </div>
          <h2>Movie</h2>
          <p>Did you watch a great movie recently? share it with us!</p>
          <p><a class="btn btn-primary" href="${pageContext.request.contextPath}/movies" role="button"><span class="glyphicon glyphicon-film"></span>Film</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <!-- <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140"> -->
          <div class='text-align-center'>
            <i class="fa fa-book custom-fa-size"></i>
          </div>
          <h2>Book</h2>
          <p>Share your thoughts about the books that you love! </p>
          <p><a class="btn btn-primary" href="${pageContext.request.contextPath}/books" role="button" ><span class="glyphicon glyphicon-book" aria-hidden="true"></span>Books</a></p>
          <!-- <p><a class="btn btn-primary" href="#" role="button" >Book</a></p> -->
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <!-- <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140"> -->
          <div class='text-align-center'>
            <i class="fa fa-headphones custom-fa-size"></i>
          </div>
          <h2>Music</h2>
          <p>Here you can share the musics that you love!</p>
          <p><a class="btn btn-primary" href="#" role="button"><span class="glyphicon glyphicon-music" aria-hidden="true"></span>Musics</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


	</jsp:attribute>
	
	<jsp:attribute name="page_footer">
      <jsp:include page="/templates/page_footer.html" />
    </jsp:attribute>
	<jsp:attribute name="javascript">
     <jsp:include page="/templates/javascript.html" />
    </jsp:attribute>
</t:base_layout> 

