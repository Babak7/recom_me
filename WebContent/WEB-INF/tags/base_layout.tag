<!DOCTYPE html>
<%@ tag description="Base layout" pageEncoding="UTF-8"%>
 
<%-- <%@ attribute name="title"%> --%>
<%@ attribute name="head_area" fragment="true" %>
<%@ attribute name="style_area" fragment="true" %>
<%@ attribute name="page_header" fragment="true" %>
<%@ attribute name="page_navbar" fragment="true" %>
<%@ attribute name="page_footer" fragment="true" %>
<%@ attribute name="content" fragment="true" %>
<%@ attribute name="javascript" fragment="true" %>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <%-- <title>${title}</title> --%>
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <jsp:invoke fragment="head_area"/>
    <!-- <style>
    		<jsp:invoke fragment="style_area"/>
    </style> -->
    


  </head>

   <body>

		<jsp:invoke fragment="page_navbar"/>
	
	    <jsp:invoke fragment="page_header"/>
	
	    <div class="container">
	
	       	<jsp:invoke fragment="content"/>
	  
	    </div> <!-- /container -->
	
	    <jsp:invoke fragment="javascript"/>
	    
	   <jsp:invoke fragment="page_footer"/>
   
   </body>
</html>